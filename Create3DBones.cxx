#include "Create3DBones.h"

// This is included here because it is forward declared in
// Create3DBones.h

#include <unordered_map>
#include <QFileDialog>
#include <QMessageBox>
#include <QScrollArea>
#include <QScrollBar>
#include <QMenu>
#include <QMenuBar>
#include <QGridLayout>
#include <QLabel>
#include <QDebug>
#include <vtkPolyDataMapper.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkSphereSource.h>
#include <vtkSmartPointer.h>
#include <vtkActor.h>
#include <vtkOBJReader.h>
#include <vtkPolyDataReader.h>
#include <vtkAxesActor.h>
#include <vtkTransform.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkDelaunay2D.h>
#include <vtkProperty.h>
#include <vtkWindowToImageFilter.h>
#include <vtkContourFilter.h>
#include <vtkCamera.h>
#include <vtkLine.h>
#include <vtkPolyDataSilhouette.h>
#include <vtkTriangle.h>
#include <vtkCleanPolyData.h>


#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkProperty.h>
#include <vtkIdList.h>
#include <vtkCell.h>
#include <vtkDataSetMapper.h>
#include <vtkIdTypeArray.h>
#include <vtkSelectionNode.h>
#include <vtkSelection.h>
#include <vtkExtractSelection.h>
#include <vtkOBJReader.h>
#include <vtkDelaunay2D.h>
#include <vtkEdgeTable.h>
#include <vtkExtractEdges.h>

#include <iostream>
#include <unordered_map>
#include <list>
#include <string>
#include <algorithm>
#include <vector>
#include <fstream>

#include <vtkSmartPointer.h>
#include <vtkMarchingCubes.h>
#include <vtkMetaImageReader.h>

#include <vtkSphereSource.h>
#include <vtkProbeFilter.h>
#include <vtkSphere.h>
#include <vtkClipDataSet.h>
#include <vtkImplicitVolume.h>
#include <vtkUnstructuredGrid.h>
#include <vtkLookupTable.h>

#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkDataSetMapper.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkActor.h>
#include <vtkCamera.h>
#include <vtkProperty.h>
#include "vtkDICOMImageReader.h"
#include <vtkSampleFunction.h>
#include <vtkCylinder.h>
#include <vtkSTLReader.h>

// For compatibility with new VTK generic data arrays
#ifdef vtkGenericDataArray_h
#define InsertNextTupleValue InsertNextTypedTuple
#endif



#include "itkMesh.h"
#include "itkMeshFileReader.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkCastImageFilter.h"
#include "itkTriangleMeshToBinaryImageFilter.h"


// Constructor
Create3DBones::Create3DBones()
{
    this->ui = new Ui_Create3DBones;
    this->ui->setupUi(this);

    /*********+ ui simulador de rayos x ****************/
    prepGraphSimXray();

    // Set up action signals and slots
    connect(this->ui->actionExit, SIGNAL(triggered()), this, SLOT(slotExit()));

    connect(this->ui->but_load_3dimage, SIGNAL(clicked()), this, SLOT(load_3dimage()));
    //simulador de rayos x
    connect(this->ui->comboBox_typeBlend,SIGNAL(currentIndexChanged(const QString&)),this,SLOT(applyBlend(const QString&)));
    connect(this->ui->but_trans_xray, SIGNAL(clicked()), this, SLOT(applyTransSimulXray()));
    //par ael cambio de spin box
    connect(this->ui->spinbox_window, SIGNAL(valueChanged(int)), this, SLOT(setWindowXray(int)));
    connect(this->ui->spinbox_level, SIGNAL(valueChanged(int)), this, SLOT(setLevelXray(int)));
    connect(this->ui->butApplyOpacity, SIGNAL(clicked()), this, SLOT(applyOpacityValues()));
    connect(this->ui->checkbox_clip, SIGNAL(clicked(bool)), this, SLOT(evalClip(bool)));


}

void Create3DBones::prepGraphSimXray(){
    listTypesBlend = new QStringList();
    listTypesBlend->append("MIP");
    listTypesBlend->append("CompositeRamp");
    listTypesBlend->append("CompositeShadeRamp");
    listTypesBlend->append("CTSkin");
    listTypesBlend->append("CTBone");
    listTypesBlend->append("CTMuscle");
    listTypesBlend->append("RGBComposite");
    //this->ui->comboBox_typeBlend->addItems(*(simXray->getlistTypesBlend()));
    this->ui->comboBox_typeBlend->addItems(*listTypesBlend);
}

QStringList * Create3DBones::getTypesBlend(){
    return listTypesBlend;
}

vtkSmartPointer<vtkRenderWindow> Create3DBones::getRenderWindow(QVTKOpenGLWidget *w){
    return w->GetRenderWindow();
}

vtkSmartPointer<vtkRenderWindowInteractor> Create3DBones::getInteractor(QVTKOpenGLWidget *w){
    return w->GetRenderWindow()->GetInteractor();
}

void Create3DBones::slotExit()
{
    qApp->exit();
}


//CARGAR MODELOS CON FORMATO SOLO VTK
void Create3DBones::cargarFileVTK(std::string filename)
{
   vtkPolyDataReader* reader = vtkPolyDataReader::New();
   reader->SetFileName(filename.c_str());
   reader->Update();

   //Visualizando
   vtkSmartPointer<vtkPolyDataMapper> mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
   mapper->SetInputConnection(reader->GetOutputPort());

   vtkSmartPointer<vtkActor> actor = vtkSmartPointer<vtkActor>::New();
   actor->SetMapper(mapper);

   vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();
   renderer->AddActor(actor);

   // VTK/Qt wedded
   this->ui->qvtkWidget_3->GetRenderWindow()->AddRenderer(renderer);
}

//menu abrir para archivos VTK para su visualizacion
void Create3DBones::load_promedio()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Open Model"), "/home", tr("Model Files (*.vtk *.obj)"));
    if (!fileName.isEmpty()) {
        cargarFileVTK(fileName.toStdString());
    }else if (fileName.isEmpty()) {
        QMessageBox::information(this, tr("Model Viewer"), tr("No puede cargar el modelos %1.").arg(fileName));
        return;
    }
}




void Create3DBones::cargar3Dimage(std::string filename){
    //creacion del simulador de rayos x
    vtkRenderWindow *renWin = getRenderWindow(this->ui->qvtkWidget_3);
    //este tipo interactor es vtkrenderwindowinteractor no es qvtkitneractors
    vtkRenderWindowInteractor *iren = renWin->GetInteractor();
    simXray = new SimuladorXray(renWin,iren);

    //asignamos la ruta de la imagen 3D a cargar
    simXray->setDirectory3Dimage(filename);

    //renderizamos la imagen 3D
    simXray->show();
}

void Create3DBones::load_3dimage(){

    QString fileName = QFileDialog::getOpenFileName(this,tr("Open Model"), "/home", tr("Model Files (*.vtk *.mha )"));
    QFileInfo fi(fileName);
    QString ext = fi.suffix();
    std::cout<<ext.toStdString();
    if (!fileName.isEmpty()) {
        cargar3Dimage(fileName.toStdString());
    }else if (fileName.isEmpty()) {
        QMessageBox::information(this, tr("Model Viewer"), tr("No puede cargar el modelos %1.").arg(fileName));
        return;
    }

    //cargamos el dicom
    /*QString fileName = QFileDialog::getExistingDirectory(this, tr("Abrir Directorio"),"/home", QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    if (!fileName.isEmpty()) {
        cargar3Dimage(fileName.toStdString());
    }else if (fileName.isEmpty()) {
        QMessageBox::information(this, tr("Model Viewer"), tr("No puede cargar el modelos %1.").arg(fileName));
        return;
    }*/

}

void Create3DBones::applyBlend(const QString& typeblend){
    if(!QString::compare(typeblend, "MIP", Qt::CaseInsensitive)){
        simXray->applyBlendMIP();
    }else if(!QString::compare(typeblend, "CompositeRamp", Qt::CaseInsensitive)){
        simXray->applyBlendCompRamp();
    }else if(!QString::compare(typeblend, "CompositeShadeRamp", Qt::CaseInsensitive)){
        simXray->applyBlendCompShadeRamp();
    }else if(!QString::compare(typeblend, "CTSkin", Qt::CaseInsensitive)){
        simXray->applyBlendCTSkin();
    }else if(!QString::compare(typeblend, "CTBone", Qt::CaseInsensitive)){
        simXray->applyBlendCTBone();
    }else if(!QString::compare(typeblend, "CTMuscle", Qt::CaseInsensitive)){
        simXray->applyBlendCTMuscle();
    }else if(!QString::compare(typeblend, "RGBComposite", Qt::CaseInsensitive)){
        simXray->applyBlendRgbComp();
    }
}

void Create3DBones::applyTransSimulXray(){
    QString rotx = this->ui->editRotX->text();
    QString roty = this->ui->editRotY->text();
    QString rotz = this->ui->editRotZ->text();
    simXray->transformRotImageData(rotx.toDouble(),roty.toDouble(),rotz.toDouble());
}

void Create3DBones::setWindowXray(int opacwin){
    simXray->setOpacityWindow(opacwin);
}
void Create3DBones::setLevelXray(int opaclev){
    simXray->setOpacityLevel(opaclev);
}

void Create3DBones::applyOpacityValues(){
    //aplicando los valores actuales de window y level
    //dependiendo del tipo de blend
    simXray->updateMapper();
}

void Create3DBones::evalClip(bool valClip){
    simXray->setFlagClip(valClip);
}


void Create3DBones::setRatFramesXray(double ratFrames){
    simXray->setRatFrames(ratFrames);
}
void Create3DBones::setFactRedXray(double factReduction){
    simXray->setFactReduction(factReduction);
}

void Create3DBones::saveVirtualXray(){
    vtkSmartPointer<vtkWindowToImageFilter> windowToImageFilter = vtkSmartPointer<vtkWindowToImageFilter>::New();
    windowToImageFilter->SetInput(simXray->getRenderWindowSimXray());
    //windowToImageFilter->SetMagnification(3); //set the resolution of the output image (3 times the current resolution of vtk render window)
    windowToImageFilter->SetInputBufferTypeToRGBA(); //also record the alpha (transparency) channel
    windowToImageFilter->ReadFrontBufferOff(); // read from the back buffer
    windowToImageFilter->Update();

    vtkSmartPointer<vtkPNGWriter> writer = vtkSmartPointer<vtkPNGWriter>::New();
    writer->SetFileName("simulatedXray.png");
    writer->SetInputConnection(windowToImageFilter->GetOutputPort());
    writer->Write();
    QMessageBox Msgbox;
    Msgbox.setText("Se guardo la imagen exitosamente");
    Msgbox.exec();
}


