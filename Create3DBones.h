#ifndef Create3DBones_H
#define Create3DBones_H

#include <vtkSmartPointer.h>
#include <QMainWindow>
#include <string>
#include "simuladorxray.h"
#include "ui_Create3DBones.h"
// Forward Qt class declarations

QT_BEGIN_NAMESPACE
class Ui_Create3DBones;
class QAction;
class QLabel;
class QMenu;
class QScrollArea;
class QScrollBar;
QT_END_NAMESPACE

class Create3DBones : public QMainWindow
{
    Q_OBJECT
public:

    // Constructor/Destructor
    Create3DBones();
    ~Create3DBones() {};

    void cargarFileVTK(std::string);
    void cargar3Dimage(std::string);

    vtkSmartPointer<vtkRenderWindow> getRenderWindow(QVTKOpenGLWidget *w);
    vtkSmartPointer<vtkRenderWindowInteractor> getInteractor(QVTKOpenGLWidget *w);


    //simulador de rayos x
    void prepGraphSimXray();
    QStringList * getTypesBlend();

public slots:

    virtual void slotExit();

private:
    // Designer form
    Ui_Create3DBones *ui;

    //Simulador de rayos x
    QStringList *listTypesBlend;
    SimuladorXray *simXray;


private slots:
    void load_promedio();

    //simulador de rayos x
    void load_3dimage();
    void applyBlend(const QString&);
    void applyTransSimulXray();
    void setLevelXray(int);
    void setWindowXray(int);
    void setRatFramesXray(double);
    void setFactRedXray(double);
    void applyOpacityValues();
    void evalClip(bool valClip);
    void loadProbeFilter();
    void saveVirtualXray();
};

#endif
