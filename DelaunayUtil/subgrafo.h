#ifndef SUBGRAFO_H
#define SUBGRAFO_H

#include <list>
#include <arista.h>
#include <vector>
#include "punto.h"

//librerias de vtk
//para el menejo de punteros sobre el polydata
#include <vtkSmartPointer.h>
//para el manejo de puntos
#include <vtkPoints.h>
//para el grafo no dirigido
#include <vtkMutableUndirectedGraph.h>
//la conversion de grafo a polydata
#include <vtkGraphToPolyData.h>
//para el mapeo del polydata a renderizacion
#include <vtkPolyDataMapper.h>
//el actor pasado al maper
#include <vtkActor.h>
//para la renderizacion
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include "utilmaths.h"

class SubGrafo
{
public:
    SubGrafo();
    SubGrafo(vector<vector<double>> listapuntos);
    void insertPunto(Punto &p);
    void doConnectionPoints();

    //conseguir los candidadtos del actual subgrafo
    void saveCandidates(Arista * lBase, Punto ptoInicio,bool esIzq);
    //filtrar candidatos que no sean la linea base
    void filtrarCandidatos(Arista *lbase,map<int,Arista,less<int>> *tmp,bool esIzq);
    //ordenar aristas candidatas
    void orderAristCand(vector<double> angulosAristas);
    //eliminar la linea base como candidato
    void eraseLineaBaseFromCandidatos(Arista *lbase,bool esIzq);


    //conseguir el menor punto de la lista de puntos del subgrafo
    void calcMenorPointInYcoord();
    Punto getMenorPtoy();

    //insertar puntos temporalmente ya que posteriormente seran reemplazados
    void insertPointsTemp(SubGrafo *s,bool esSubIzq);
    void insertArista(int idOrigen, int idDestino);

    void printSubgrafo();
    void printAristaSubGra();
    void drawSubgrafo(vtkMutableUndirectedGraph *g,vtkPoints *points);
    void createIdsPuntosSubgrafo(vtkSmartPointer<vtkMutableUndirectedGraph> g);
    void printIdes();
    vector<Punto> getGroupPuntos();
    vector<Punto>* getRefGroupPuntos();
    vector<Arista> getListArisCand();
    int getNroPtos();
    int getNroPtosReal();
    void setNroPtos(int nroPtos);
    //    bool doFirstCond();
    //    void anguloEnAristas(int idAristaA, int idAristaB);
    //    bool doSecondCond();
    //    bool pointInCircle();
    //    void completarDelaunay();
    //    //retorna id de aristas(
    //    std::list<int> buscarAristasConPuntoOrigen(int idPunto);
    //    void agregarArista();
    //    void eliminarArista();
    //    //retorna id de punto
    //    int getCommonPointEnAristas(int idAristaA, int idAristaB);
private:

    //grupo de puntos del subgrafo
    vector<Punto> group_puntos;

    //lista de aristas candidatas del subgrafo
    vector<Arista> listArisCand;
    //map<int, Arista, std::less<int>> listArisCand;

    //indice actual de la lista de aristas candidatas
    int iterActAC = 0;

    //Menor punto en coordenada y
    Punto minPtoy;

    //Final Candidato
    Punto finalCandidato;

    //Nro de puntos por subgrafo
    int nroPtos;
};

#endif // SUBGRAFO_H
